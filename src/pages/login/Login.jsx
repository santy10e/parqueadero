import React, { useState } from "react";
import { Link, useNavigate, Navigate } from "react-router-dom";
import { useAuth } from "../../context/authContext";

import "./login.scss";

function Login() {
  const { login, currentUser } = useAuth();
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const { email, password } = formData;
    await login(email, password);
  };

  // If the user is already logged in, redirect to Home
  if (currentUser) {
    return <Navigate to="/" />;
  }

  return (
    <div className="login">
      <div className="card">
        <div className="right">
          <h1>Login</h1>
          <form onSubmit={handleSubmit}>
            <input
              type="email"
              name="email"
              placeholder="Email"
              onChange={handleInputChange}
              required
            />
            <input
              type="password"
              name="password"
              placeholder="Password"
              onChange={handleInputChange}
              required
            />
            <button type="submit">Login</button>
          </form>
        </div>
        <div className="left">
          <h1>Registro</h1>
          <p>
            ¿No tienes cuenta aún? 
          </p>
          <span>Si aún no te has registrado</span>
          <Link to="/register">
            <button>Registrarse</button>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Login;
